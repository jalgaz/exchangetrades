#!/usr/bin/env bash
DST_DIR="${PWD}/venv"
PYTHON_VERSION="python3"
REQ_FILE="requirements.txt"

if [ -d "$DST_DIR" ]; then
  echo "--> Deleting old $DST_DIR ..."
  rm -rf $DST_DIR
fi

echo "--> Creating virtualenv ..."
virtualenv --python=$PYTHON_VERSION $DST_DIR

if [ -e "$REQ_FILE" ]; then
  echo "--> Install requirements from $REQ_FILE ..."
  $DST_DIR/bin/pip install -r `pwd`/$REQ_FILE
fi
