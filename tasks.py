from invoke import run, task

VENV_BIN = 'venv/bin'
PRJ_DIR = 'exchangetrades_prj'

@task
def clean(ctx, docs=False, bytecode=False, extra=''):
    patterns = ['build']
    if docs:
        patterns.append('docs/_build')
    if bytecode:
        patterns.append('**/*.pyc')
    if extra:
        patterns.append(extra)
    for pattern in patterns:
        ctx.run("rm -rf {0}".format(pattern))

@task
def test(ctx):
    """Run Django tests and linters."""
    run("{}/python exchangetrades_prj/manage.py test".format(VENV_BIN))

@task
def update(ctx):
    """Update local project based on upstream changes."""
    print("Updating requirements...")
    run("{}/pip install -r requirements.txt".format(VENV_BIN))
    run("{}/python {}/manage.py makemigrations".format(VENV_BIN, PRJ_DIR))
    run("{}/python {}/manage.py migrate".format(VENV_BIN, PRJ_DIR))
    #run("{}/python {}/manage.py collectstatic --noinput".format(VENV_BIN, PRJ_DIR))
