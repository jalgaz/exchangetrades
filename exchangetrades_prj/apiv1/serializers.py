from trades.models import ExchangeTrades
from rest_framework import serializers


class TradesSerializer(serializers.ModelSerializer):
    """
    Serializer to ExchangeTrades model.
    """

    class Meta:
        model = ExchangeTrades
        fields = ('id', 'sell_currency', 'sell_amount',
                  'buy_currency', 'buy_amount',
                  'rate', 'date_booked')


