from django.conf.urls import url, include
from rest_framework import routers
from apiv1.viewsets import TradesViewSet

router = routers.DefaultRouter()
router.register(r'exchangetrades', TradesViewSet)

urlpatterns = [
    url(r'^', include(router.urls))
]
