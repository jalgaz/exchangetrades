from trades.models import ExchangeTrades
from rest_framework import permissions, viewsets

from apiv1.serializers import TradesSerializer


class TradesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allow ExchangeTrades to be viewed
    """
    permission_classes = (permissions.AllowAny, )
    queryset = ExchangeTrades.objects.all().order_by('-date_booked')
    serializer_class = TradesSerializer
