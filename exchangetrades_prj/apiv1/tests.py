from django.test import TestCase
from decimal import Decimal

from trades.models import ExchangeTrades


class ExchangeTradesTest(TestCase):
    """ Test module for ExchangeTrades model """

    def setUp(self):
        self.SELL_CURRENCY = 'USD'
        self.SELL_AMOUNT = round(Decimal(100.0), 2)
        self.BUY_CURRENCY = 'EUR'
        self.BUY_AMOUNT = round(Decimal(83.70), 2)
        self.RATE = round(Decimal(0.84), 5)
        ExchangeTrades.objects.create(
            sell_currency=self.SELL_CURRENCY, sell_amount=self.SELL_AMOUNT,
            buy_currency=self.BUY_CURRENCY, buy_amount=self.BUY_AMOUNT,
            rate=self.RATE)


    def test_exchangetrades(self):
        exchange_trades = ExchangeTrades.objects.all()
        for trans in exchange_trades:
            self.assertTrue(trans.sell_currency == self.SELL_CURRENCY)
            self.assertTrue(trans.sell_amount == self.SELL_AMOUNT)
            self.assertTrue(trans.buy_currency == self.BUY_CURRENCY)
            self.assertTrue(trans.buy_amount == self.BUY_AMOUNT)
            self.assertTrue(trans.rate == self.RATE)
