from django.db import models, IntegrityError
from django.conf import settings

import random
import string

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


TABLE_NAME = 'trades_exchangetrades'

class ExchangeTrades(models.Model):
    """
    Model to represent Foreign Exchange Trades
    """


    def generate_random_id(length=7):
        """
        Generate a random id, alphanumeric.
        :param length: lenght of string to return (default 7 characters)
        :return: string 'TR' + random value
        """
        random_value = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))
        if hasattr(settings, 'ID_SEQUENCE_PREFIX') and settings.ID_SEQUENCE_PREFIX:
            return settings.ID_SEQUENCE_PREFIX + random_value
        else:
            return random_value
        

    id = models.CharField('ID', primary_key=True, max_length=9,
                          default=generate_random_id, editable=False)
    sell_currency = models.CharField('Sell Currency', max_length=3)
    sell_amount = models.DecimalField('Sell Amount', max_digits=10, decimal_places=2)
    buy_currency = models.CharField('Buy Currency', max_length=3)
    buy_amount = models.DecimalField('Buy Amount', max_digits=10, decimal_places=2)
    rate = models.DecimalField('Rate', max_digits=7, decimal_places=5)
    date_booked = models.DateTimeField(auto_now_add=True)


    def save(self, *args, **kwargs):
        """
        Try to detect collisions (random field id) and provide new id.
        We log all collisions.
        """
        while True:
            try:
                super(ExchangeTrades, self).save(*args, **kwargs)
                break
            except IntegrityError as e:
                logger.warning('Collision on ExchangeTrades ID')
                if TABLE_NAME + '.id' in str(e):
                    self.id = self.generate_random_id()



    class Meta:
        # necessary to ensure that the table does not change and thus check for collisions
        db_table = TABLE_NAME
