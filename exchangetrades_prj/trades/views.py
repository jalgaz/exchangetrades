from django.shortcuts import render


def HomeView(request):
    """
    Render Home 
    """
    return  render(request, 'index.html')


def NewTradeView(request):
    """
    Render page to create new trade 
    """
    return  render(request, 'newtrade.html')
