# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-16 11:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trades', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='exchangetrades',
            name='transaction_id',
            field=models.CharField(default='TRTsTgdgz', max_length=9, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='exchangetrades',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
