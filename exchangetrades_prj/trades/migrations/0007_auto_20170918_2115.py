# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-18 21:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trades', '0006_auto_20170916_1228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exchangetrades',
            name='rate',
            field=models.DecimalField(decimal_places=5, max_digits=7, verbose_name='Rate'),
        ),
    ]
