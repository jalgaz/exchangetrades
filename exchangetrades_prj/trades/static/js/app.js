const fixerUrl = 'http://api.fixer.io/latest';

const serverUrl = window.location 
const apiv1Url =  serverUrl + 'apiv1/exchangetrades/'
const newTradeUrl = serverUrl + 'newtrade'


function getTable() {
    // this function generate table structure
    var table = `
        <table class="table">
        <thead>
          <tr>
            <th>Sell CCY</th>
            <th>Sell Amount</th>
            <th>Buy CCY</th>
            <th>Buy Amount</th>
            <th>Rate</th>
            <th>Date Booked <a href="https://es.wikipedia.org/wiki/Tiempo_universal_coordinado">(UTC)</a></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>`;
    return table;
}

function createRow(data) {
    // make a table row from data
    var row = '<tr> <td>' + data.sell_currency + '</td>';
    var row = row + '<td class="text-right">' + data.sell_amount + '</td>';
    var row = row + '<td>' + data.buy_currency + '</td>';
    var row = row + '<td class="text-right">' + data.buy_amount + '</td>';
    var row = row + '<td class="text-right">' + data.rate + '</td>';
    var row = row + '<td>' + data.date_booked + '</td></tr>';
    return row;
}

function openCreateTrade() {
    // Button create to new page
    window.location.href = newTradeUrl;

}

$(document).ready(function(){
    // fetch api data
    fetch(apiv1Url)
    .then( resp => resp.json() )
    .then( jsondata => {
        $('#table1').html(getTable());
        jsondata.results.forEach(function(element) {
            jQuery('#table1 tbody').append(createRow(element));   
        });
    })
    .catch(function(error) {
        alert('Error on fetch from API: ' + error);
    });

});
