/*

     js to createtrade.html page
*/

const fixerUrl = 'http://api.fixer.io/latest';

const rootUrl = window.location.protocol + '//' + window.location.host;

const apiv1TradesUrl =  rootUrl + '/apiv1/exchangetrades/'

// store the actual rate for the selected currencies or null if not have selected from dropdowns
var rate = null;

function createOptionNode(currency_name) {
    // create a Option node under the <select> by id=select_id (create currencies options)
    var row = '<option>' + currency_name + '</option>'
}


function setCurrenciesDropdowns() {
    // create currencies dropdown options from Fixer.io
    fetch(fixerUrl)
    .then( resp => resp.json() )
    .then( jsondata =>  {
        // add manually EUR
        $('#sell_currencies, #buy_currencies').append($("<option>EUR</option>"));
        // now rest of currencies
        $.each(jsondata.rates, function(key, value) {
             $('#sell_currencies, #buy_currencies').append($("<option></option>")
                                                    .attr("value",key)
                                                    .text(key));
        });
    })
    .catch(function(error) {
        alert('Error on fetch from API: ' + error);
    });
}


// Fetch rate from fixer.io api to selected currencies
function getRate(sell_currency, buy_currency) {
    if (sell_currency && buy_currency) {
        var rateUrl = fixerUrl + '?base=' + sell_currency + '&symbols=' + buy_currency
        fetch(rateUrl)
        .then( resp => resp.json() )
        .then( jsondata =>  {
            $.each(jsondata.rates, function(key, val) {
                rate = val ? val : 1;
                $('#rate').text(rate);
            });
        })
        .catch(function(error) {
            alert('Error on fetch from API: ' + error);
        });
    }
}


// detect changes on selected dropdown choices and get rate from api.fixer.io
$('#sell_currencies, #buy_currencies')
  .change(function () {
    $('#sell_amount, buy_amount').val(null);
    if ($('#sell_currencies option:selected').text() != '' &&
        $('#buy_currencies option:selected').text() != '') {
        $('rate').text(getRate(
            $('#sell_currencies option:selected').text(),
            $('#buy_currencies option:selected').text(),
        ));
    } else {
        rate = null;
        $('rate').text('--')
    }
  })
  .change();


// every input on sell_amount field change value on buy_amount
$('#sell_amount').on('input', function(e) {
    if (rate) {
        var sell_amount = $('#sell_amount').val();
        var buy_amount = parseFloat( sell_amount ) * rate;
        $('#buy_amount').val(buy_amount.toFixed(2));
    }
});


// check if can send data to api o maybe some fields are no valid
function validateData() {
    var res = true;
    if ($('#sell_currencies').val() &&
        $('#sell_amount').val() &&
        $('#buy_currencies').val() &&
        $('#buy_amount').val() &&
        rate) {
            return true;
        } else {
            return false;
        }
}

// send data to api and create a new record
function createTrade() {
    if (!validateData()) return;
    var sell_amount = $('#sell_amount').val();
    //sell_amount = sell_amount.toFixed(2);
    var payload = {
        'sell_currency': $('#sell_currencies').val(),
        'sell_amount': parseFloat( parseFloat(sell_amount).toFixed(2) ),
        'buy_currency': $('#buy_currencies').val(),
        'buy_amount': parseFloat($('#buy_amount').val()),
        'rate': parseFloat( parseFloat(rate).toFixed(2) )
    };
    fetch(apiv1TradesUrl,
    {
        method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(payload)
    })
    .then( resp => resp.json() )
    .then( jsondata =>  {
        //alert('Transaction ID: ' + jsondata.id + ' on date: ' + jsondata.date_booked);
        window.location.href = rootUrl;
    })
    .catch( data => { alert( JSON.stringify( data ) ) })


}

$(document).ready(function(){
    setCurrenciesDropdowns();
});
