#!/usr/bin/env bash

BASE_DIR=`pwd`
VENV_PATH="../venv/bin"
PRJ_NAME="exchangetrades_prj"
WSGI="wsgi"

BIND_ADDRESS="0.0.0.0:8000"
WORKERS=2

echo "Arrancando gunicorn..."
$VENV_PATH/gunicorn -w $WORKERS -b $BIND_ADDRESS --chdir $BASE_DIR  $PRJ_NAME.$WSGI